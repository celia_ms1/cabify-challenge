# Cabify Challenge

This project simulates a physical Cabify's store which sells 3 products. You can try it in [Cabify Challenge](https://celia_ms1.gitlab.io/cabify-challenge/)

## Dashboard

On this screen you could:

- Add/remove products in the cart.
- Scan products.
- Checkout the cart.
- View a modal with information about the product selected.
- Change the language.

![image-1.png](./image-1.png)

### Toolbar

In the toolbar you could click in the logo of `Setting` and bring up a menu with one option:

- Languages: that Change the language of the application.

![image-2.png](./image-2.png)

### Scan

With this button you could simulate that add three news product in the cart (two TSHIRT and one CAP).

![image-3.png](./image-3.png)

### Checkout

With this button you could simulate that pay and clear the cart.

![image-4.png](./image-4.png)

### Modal

With this button you could view a modal with information about the product selected and add it in the cart.

![image-5.png](./image-5.png)

![image-6.png](./image-6.png)

## Responsive

### iPad Air

![image-9.png](./image-9.png) ![image-10.png](./image-10.png)

### Samsung Galaxy S8

![image-7.png](./image-7.png) ![image-8.png](./image-8.png)

# Important things

## External libraries used

- [NgRx](https://ngrx.io) to manage the application state, based on the Redux pattern.
- [lodash](https://lodash.com) to working with arrays, numbers, objects, strings, etc.

## UI component libraries used

- [Material](https://material.angular.io) to create UI component infrastructure and Material Design components for mobile and desktop Angular web applications.
- [Bootstrap utility classes](https://getbootstrap.com/docs/5.1/utilities) to showing, hiding, aligning, and spacing content.

## Unit and Integration testing results

- FooterComponent (HTML test)
- CarService

![image.png](./image.png)

# Running application

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 12.0.2.

## Development server

Run `ng serve` or `npm start` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` or `npm build` to build the project. The build artifacts will be stored in the `dist/` directory.

## Running unit tests

Run `ng test` or `npm test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via a platform of your choice. To use this command, you need to first add a package that implements end-to-end testing capabilities.

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI Overview and Command Reference](https://angular.io/cli) page.
