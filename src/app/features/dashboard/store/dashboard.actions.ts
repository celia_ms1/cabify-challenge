import { createAction, props, union } from '@ngrx/store';
import { CartItem } from 'src/app/core/models/cart-item.model';

export const getCart = createAction('[DASHBOARD] Get cart');

export const updateCart = createAction(
  '[DASHBOARD] Update cart',
  props<{ cart: CartItem[] }>()
);

export const clearDashboardState = createAction('[DASHBOARD] Clear state');

const actions = union({
  getCart,
  updateCart,
  clearDashboardState,
});

export type DashboardActions = typeof actions;
