import { createSelector, createFeatureSelector } from '@ngrx/store';
import * as dashboardReducer from './dashboard.reducers';

export const getDashboardState =
  createFeatureSelector<dashboardReducer.DashboardState>('dashboard');

export const getCart = createSelector(
  getDashboardState,
  (state: dashboardReducer.DashboardState) => state.cart
);
