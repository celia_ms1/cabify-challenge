import { Action, createReducer, on } from '@ngrx/store';
import { cartMock } from 'src/app/core/mocks/cart.mock';
import { CartItem } from 'src/app/core/models/cart-item.model';
import { getCart, updateCart, clearDashboardState } from './dashboard.actions';

export interface DashboardState {
  cart: CartItem[];
}

export const initialState: DashboardState = {
  cart: cartMock,
};

const _dashboardReducer = createReducer(
  initialState,
  on(getCart, (state) => {
    return {
      ...state,
    };
  }),
  on(updateCart, (state, { cart }) => {
    return {
      ...state,
      cart: cart,
    };
  }),
  on(clearDashboardState, () => {
    return {
      ...initialState,
    };
  })
);

export function DashboardReducer(
  state: DashboardState | undefined,
  action: Action
) {
  return _dashboardReducer(state, action);
}
