import { Component, ComponentFactoryResolver, OnInit } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { Subscription } from 'rxjs';
import { AppState } from 'src/app/core/store/app.state';
import {
  clearDashboardState,
  getCart,
  updateCart,
} from '../../store/dashboard.actions';
import * as dashboardSelector from 'src/app/features/dashboard/store/dashboard.selector';
import * as _ from 'lodash';
import { CheckoutService } from 'src/app/core/services/checkout.service';
import { ProductType } from '../../../../core/models/product.model';
import { SnackBarService } from 'src/app/core/services/snackbar.service';
import { CartItem } from 'src/app/core/models/cart-item.model';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss'],
})
export class DashboardComponent implements OnInit {
  subscriptions = new Subscription();

  cart: CartItem[] = [];

  constructor(
    private snackBarService: SnackBarService,
    private checkoutService: CheckoutService,
    private store: Store<AppState>
  ) {
    this.subscriptions.add(
      this.store.pipe(select(dashboardSelector.getCart)).subscribe((cart) => {
        this.cart = _.cloneDeep(cart);
      })
    );

    this.subscriptions.add(
      this.checkoutService.scanProduct.subscribe((productType) => {
        if (productType) {
          const cartItem = _.find(
            this.cart,
            (c) => c.product.type === productType
          );
          if (cartItem) {
            cartItem.quantity++;
            this.store.dispatch(updateCart({ cart: this.cart }));
            this.snackBarService.success('success.scan');
          }
        }
      })
    );
  }

  ngOnInit(): void {
    this.store.dispatch(getCart());
  }

  scanProducts() {
    this.checkoutService
      .scan(ProductType.TSHIRT)
      .scan(ProductType.CAP)
      .scan(ProductType.TSHIRT);
  }

  checkoutCart() {
    this.store.dispatch(clearDashboardState());
    this.snackBarService.success('success.checkout');
  }

  ngOnDestroy(): void {
    this.store.dispatch(clearDashboardState());
    this.subscriptions.unsubscribe();
  }
}
