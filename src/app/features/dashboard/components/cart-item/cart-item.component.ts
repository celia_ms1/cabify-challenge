import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { paths } from 'src/app/app-paths';
import { CartItem } from 'src/app/core/models/cart-item.model';

@Component({
  selector: 'app-cart-item',
  templateUrl: './cart-item.component.html',
  styleUrls: ['./cart-item.component.scss'],
})
export class CartItemComponent implements OnInit {
  @Input('cartItem') cartItem!: CartItem;

  @Output() addClick = new EventEmitter();
  @Output() removeClick = new EventEmitter();
  @Output() openDialogClick = new EventEmitter();

  pathImgProduct = paths.image_products;

  constructor() {}

  ngOnInit(): void {}

  addItem() {
    this.cartItem.quantity++;
    this.addClick.emit();
  }

  removeItem() {
    if (this.cartItem.quantity > 0) {
      this.cartItem.quantity--;
      this.removeClick.emit();
    }
  }
}
