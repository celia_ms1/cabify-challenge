import {
  Component,
  ComponentFactoryResolver,
  Input,
  OnInit,
  ViewChild,
  ViewContainerRef,
} from '@angular/core';
import { Store } from '@ngrx/store';
import { CartItem } from 'src/app/core/models/cart-item.model';
import { Product } from 'src/app/core/models/product.model';
import { SnackBarService } from 'src/app/core/services/snackbar.service';
import { AppState } from 'src/app/core/store/app.state';
import { ProductDialogComponent } from 'src/app/shared/product-dialog/product-dialog.component';
import { updateCart } from '../../store/dashboard.actions';

@Component({
  selector: 'app-shopping-cart',
  templateUrl: './shopping-cart.component.html',
  styleUrls: ['./shopping-cart.component.scss'],
})
export class ShoppingCartComponent implements OnInit {
  @Input('cart') cart!: CartItem[];

  @ViewChild('productDialog', { read: ViewContainerRef })
  productDialog!: ViewContainerRef;

  constructor(
    private snackBarService: SnackBarService,
    private componentFactoryResolver: ComponentFactoryResolver,
    private store: Store<AppState>
  ) {}

  ngOnInit(): void {}

  updateCart() {
    this.store.dispatch(updateCart({ cart: this.cart }));
  }

  openDialog(product: Product) {
    const componentFactory =
      this.componentFactoryResolver.resolveComponentFactory(
        ProductDialogComponent
      );
    const component = this.productDialog.createComponent(componentFactory);
    component.instance.open();
    component.instance.product = product;
    component.instance.addClick.subscribe(() => {
      const cartItem = this.cart.find((c) => c.product.type === product.type);
      if (cartItem) {
        cartItem.quantity++;
        this.updateCart();
        this.productDialog.clear();
        this.snackBarService.success('success.add');
      }
    });
    component.instance.closeClick.subscribe(() => {
      this.productDialog.clear();
    });
  }
}
