import {
  Component,
  EventEmitter,
  Input,
  OnChanges,
  OnInit,
  Output,
  SimpleChanges,
} from '@angular/core';
import { CartItem } from 'src/app/core/models/cart-item.model';
import { Promotion } from 'src/app/core/models/promotions/promotion.interface';
import { CheckoutService } from 'src/app/core/services/checkout.service';

@Component({
  selector: 'app-summary',
  templateUrl: './summary.component.html',
  styleUrls: ['./summary.component.scss'],
})
export class SummaryComponent implements OnInit, OnChanges {
  @Input('cart') cart!: CartItem[];
  @Output() checkoutClick = new EventEmitter();

  promotions!: Promotion[];

  totalItems: number = 0;
  totalItemsPrice: number = 0;
  total: number = 0;

  constructor(private checkoutService: CheckoutService) {
    this.promotions = this.checkoutService.getPromotions();
  }

  ngOnInit(): void {}

  ngOnChanges(changes: SimpleChanges) {
    for (const propName in changes) {
      if (propName === 'cart') {
        this.totalItems = this.checkoutService.totalItems(this.cart);
        this.totalItemsPrice = this.checkoutService.totalItemsPrice(this.cart);
        this.total = this.checkoutService.total(this.cart);
      }
    }
  }

  getDiscount(promotion: Promotion) {
    const discount = promotion.getDiscount(this.cart);
    return discount > 0 ? `-${discount}€` : `${discount}€`;
  }
}
