import { ActionReducerMap } from '@ngrx/store';
import {
  DashboardReducer,
  DashboardState,
} from 'src/app/features/dashboard/store/dashboard.reducers';

export interface AppState {
  dashboard: DashboardState;
}

export const appReducers: ActionReducerMap<AppState> = {
  dashboard: DashboardReducer,
};
