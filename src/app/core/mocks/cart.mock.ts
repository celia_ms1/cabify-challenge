import { CartItem } from '../models/cart-item.model';
import { productsMock } from './products.mock';

export const cartMock: CartItem[] = [
  {
    id: 1,
    product: productsMock[0],
    quantity: 0,
  },
  {
    id: 2,
    product: productsMock[1],
    quantity: 0,
  },
  {
    id: 3,
    product: productsMock[2],
    quantity: 0,
  },
];
