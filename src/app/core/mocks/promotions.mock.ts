import { ProductType } from '../models/product.model';
import { Promotion2For1 } from '../models/promotions/promotion-2-for-1.model';
import { PromotionBulk } from '../models/promotions/promotion-bulk.model';
import { Promotion } from '../models/promotions/promotion.interface';

export const promotionsMock: Promotion[] = [
  new Promotion2For1('2x1 Mug', ProductType.MUG),
  new PromotionBulk('3 T-shirt 5%', ProductType.TSHIRT, 5, 3),
];
