import { paths } from 'src/app/app-paths';
import { Product, ProductType } from '../models/product.model';

export const productsMock: Product[] = [
  {
    id: 1,
    name: 'Cabify T-Shirt',
    type: ProductType.TSHIRT,
    code: 'X7R2OPX',
    description:
      'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Maecenas porttitor congue massa. Fusce posuere, magna sed pulvinar ultricies, purus lectus malesuada libero, sit amet commodo magna eros quis urna. Nunc viverra imperdiet enim. Fusce est. Vivamus a tellus. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.',
    price: 20,
    image: 'shirt.png',
  },
  {
    id: 2,
    name: 'Cabify Coffee Mug',
    type: ProductType.MUG,
    code: 'X2G2OPZ',
    description:
      'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Maecenas porttitor congue massa. Fusce posuere, magna sed pulvinar ultricies, purus lectus malesuada libero, sit amet commodo magna eros quis urna. Nunc viverra imperdiet enim. Fusce est. Vivamus a tellus. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.',
    price: 5,
    image: 'mug.png',
  },
  {
    id: 3,
    name: 'Cabify Cap',
    type: ProductType.CAP,
    code: 'X3W2OPY',
    description:
      'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Maecenas porttitor congue massa. Fusce posuere, magna sed pulvinar ultricies, purus lectus malesuada libero, sit amet commodo magna eros quis urna. Nunc viverra imperdiet enim. Fusce est. Vivamus a tellus. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.',
    price: 10,
    image: 'cap.png',
  },
];
