import { CartItem } from '../cart-item.model';
import { ProductType } from '../product.model';

export interface Promotion {
  name: String;
  productType: ProductType;

  isApplicable(cart: CartItem[]): Boolean;
  getDiscount(cart: CartItem[]): number;
}
