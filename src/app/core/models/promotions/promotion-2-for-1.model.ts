import { CartItem } from '../cart-item.model';
import { ProductType } from '../product.model';
import { Promotion } from './promotion.interface';

export class Promotion2For1 implements Promotion {
  productType: ProductType;
  name: String;

  cartItem: CartItem | undefined;

  constructor(name: String, productType: ProductType) {
    this.name = name;
    this.productType = productType;
  }

  isApplicable(cart: CartItem[]): Boolean {
    this.cartItem = cart.find((c) => c.product.type === this.productType);
    return this.cartItem && Math.trunc(this.cartItem.quantity / 2) > 0
      ? true
      : false;
  }

  getDiscount(cart: CartItem[]): number {
    if (this.isApplicable(cart) && this.cartItem) {
      return (
        Math.trunc(this.cartItem.quantity / 2) * this.cartItem.product.price
      );
    }
    return 0;
  }
}
