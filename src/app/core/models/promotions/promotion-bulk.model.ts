import { CartItem } from '../cart-item.model';
import { ProductType } from '../product.model';
import { Promotion } from './promotion.interface';

export class PromotionBulk implements Promotion {
  name: String;
  productType: ProductType;

  cartItem: CartItem | undefined;
  percentage!: number;
  quantity!: number;

  constructor(
    name: String,
    productType: ProductType,
    percentage: number,
    quantity: number
  ) {
    this.name = name;
    this.productType = productType;
    this.percentage = percentage;
    this.quantity = quantity;
  }

  isApplicable(cart: CartItem[]): Boolean {
    this.cartItem = cart.find((c) => c.product.type === this.productType);
    return this.cartItem && this.cartItem.quantity >= this.quantity
      ? true
      : false;
  }

  getDiscount(cart: CartItem[]): number {
    if (this.isApplicable(cart) && this.cartItem) {
      return this.getPercentage(
        this.cartItem.product.price * this.cartItem.quantity
      );
    }
    return 0;
  }

  private getPercentage(num: number): number {
    return (num / 100) * this.percentage;
  }
}
