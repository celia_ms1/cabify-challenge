export enum ProductType {
  TSHIRT = 'Tshirt',
  MUG = 'Mug',
  CAP = 'Cap',
}

export interface Product {
  id: number;
  name: string;
  type: ProductType;
  code: string;
  description: string;
  price: number;
  image: string;
}
