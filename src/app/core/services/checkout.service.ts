import { Injectable } from '@angular/core';
import * as _ from 'lodash';
import { promotionsMock } from '../mocks/promotions.mock';
import { BehaviorSubject } from 'rxjs';
import { CartItem } from '../models/cart-item.model';

@Injectable({
  providedIn: 'root',
})
export class CheckoutService {
  scanProduct: BehaviorSubject<string> = new BehaviorSubject<string>('');

  constructor() {}

  /**
   * Scans a product adding it to the current cart.
   * @param code The product identifier
   * @returns itself to allow function chaining
   */
  scan(code: string): this {
    this.scanProduct.next(code);
    return this;
  }

  /**
   * Get all promotions.
   */
  getPromotions() {
    return promotionsMock;
  }

  /**
   * Returns the total items.
   */
  totalItems(cart: CartItem[]): number {
    return (
      _.reduce(
        _.map(cart, (cartItem) => cartItem.quantity),
        (accumulator, curr) => accumulator + curr
      ) || 0
    );
  }

  /**
   * Returns the price of all items without discount.
   */
  totalItemsPrice(cart: CartItem[]): number {
    return (
      _.reduce(
        _.map(cart, (cartItem) => cartItem.product.price * cartItem.quantity),
        (accumulator, curr) => accumulator + curr
      ) || 0
    );
  }

  /**
   * Returns the value of all discounts.
   */
  totalDiscounts(cart: CartItem[]): number {
    return (
      _.reduce(
        _.map(this.getPromotions(), (promotion) => promotion.getDiscount(cart)),
        (accumulator, curr) => accumulator + curr
      ) || 0
    );
  }

  /**
   * Returns the value of all cart products with the discounts applied.
   */
  total(cart: CartItem[]): number {
    return this.totalItemsPrice(cart) - this.totalDiscounts(cart) || 0;
  }
}
