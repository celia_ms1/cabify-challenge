import { TestBed } from '@angular/core/testing';
import { promotionsMock } from '../mocks/promotions.mock';
import { CheckoutService } from './checkout.service';
import { Promotion2For1 } from '../models/promotions/promotion-2-for-1.model';
import { PromotionBulk } from '../models/promotions/promotion-bulk.model';
import { ProductType } from '../models/product.model';
import { CartItem } from '../models/cart-item.model';
import { productsMock } from '../mocks/products.mock';

describe('CheckoutService', () => {
  let service: CheckoutService;

  const cartMockTest: CartItem[] = [
    {
      id: 1,
      product: productsMock[0],
      quantity: 3,
    },
    {
      id: 2,
      product: productsMock[1],
      quantity: 4,
    },
    {
      id: 3,
      product: productsMock[2],
      quantity: 4,
    },
  ];

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CheckoutService],
    });
    service = new CheckoutService();
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  describe('checkout functions', () => {
    describe('scan function', () => {
      it('should have scan function', () => {
        expect(service.scan).toBeTruthy();
      });

      it('should return CheckoutService', () => {
        const checkoutService = service.scan(ProductType.MUG);
        expect(checkoutService instanceof CheckoutService).toBe(
          true,
          'instance of CheckoutService'
        );
      });
    });
    describe('getPromotions function', () => {
      it('should have getPromotions function', () => {
        expect(service.getPromotions).toBeTruthy();
      });

      it('should return all promotions', () => {
        const promotions = service.getPromotions();
        expect(promotionsMock).toEqual(promotions);
      });

      it('should return Promotion[]', () => {
        const promotions = service.getPromotions();
        promotions.forEach((promotion) => {
          expect(
            promotion instanceof Promotion2For1 ||
              promotion instanceof PromotionBulk
          ).toBe(true, 'instance of Promotion');
        });
      });

      it('should return an array length > 0', () => {
        const promotions = service.getPromotions();
        expect(promotions.length).toBeGreaterThan(0);
      });
    });
    describe('totalItems function', () => {
      it('should have totalItems function', () => {
        expect(service.totalItems).toBeTruthy();
      });

      it('should return the total items', () => {
        const totalItems = service.totalItems(cartMockTest);
        expect(totalItems).toBe(11);
      });

      it('should return 0 if the cart is empty', () => {
        const totalItems = service.totalItems([]);
        expect(totalItems).toBe(0);
      });
    });

    describe('totalItemsPrice function', () => {
      it('should have totalItemsPrice function', () => {
        expect(service.totalItemsPrice).toBeTruthy();
      });

      it('should return the price of all items without discount', () => {
        const totalItemsPrice = service.totalItemsPrice(cartMockTest);
        expect(totalItemsPrice).toBe(120);
      });

      it('should return 0 if the cart is empty', () => {
        const totalItemsPrice = service.totalItemsPrice([]);
        expect(totalItemsPrice).toBe(0);
      });
    });

    describe('totalDiscounts function', () => {
      it('should have totalDiscounts function', () => {
        expect(service.totalDiscounts).toBeTruthy();
      });

      it('should return the value of all discounts', () => {
        const totalDiscounts = service.totalDiscounts(cartMockTest);
        expect(totalDiscounts).toBe(13);
      });

      it('should return 0 if the cart is empty', () => {
        const totalDiscounts = service.totalDiscounts([]);
        expect(totalDiscounts).toBe(0);
      });
    });

    describe('total function', () => {
      it('should have total function', () => {
        expect(service.total).toBeTruthy();
      });

      it('should return the value of all cart products with the discounts applied', () => {
        const total = service.total(cartMockTest);
        expect(total).toBe(107);
      });

      it('should return 0 if the cart is empty', () => {
        const total = service.total([]);
        expect(total).toBe(0);
      });
    });
  });
});
