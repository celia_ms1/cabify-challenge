import {
  Component,
  EventEmitter,
  Input,
  OnInit,
  Output,
  ViewChild,
} from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { paths } from 'src/app/app-paths';
import { Product } from 'src/app/core/models/product.model';

@Component({
  selector: 'app-product-dialog',
  templateUrl: './product-dialog.component.html',
  styleUrls: ['./product-dialog.component.scss'],
})
export class ProductDialogComponent implements OnInit {
  @ViewChild('dialog', { static: true }) dialog: any;
  dialogRef!: MatDialogRef<any>;

  @Input('product') product!: Product;

  @Output() closeClick = new EventEmitter();
  @Output() cancelClick = new EventEmitter();
  @Output() addClick = new EventEmitter();

  pathImgXLProduct = paths.image_products_xl;

  constructor(public matDialog: MatDialog) {}

  ngOnInit(): void {}

  open(): void {
    this.dialogRef = this.matDialog.open(this.dialog, {
      width: '60vw',
      height: '80vh',
      backdropClass: 'product-dialog-container',
    });
  }

  cancel() {
    this.cancelClick.emit();
    this.dialogRef.close();
  }

  add() {
    this.addClick.emit();
  }

  success() {
    this.dialogRef.close();
  }

  close() {
    this.closeClick.emit();
    this.dialogRef.close();
  }
}
