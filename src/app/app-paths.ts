export const paths = {
  dashboard: 'dashboard',
  i18n: 'assets/i18n/',
  images: '../assets/img',
  image_flags: 'assets/img/flags',
  image_products: 'assets/img/products/',
  image_products_xl: 'assets/img/products/XL/',
};
